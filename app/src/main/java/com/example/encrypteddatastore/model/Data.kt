package com.example.encrypteddatastore.model

import kotlinx.serialization.Serializable

@Serializable
data class Data(
  val value: String? = null,
  val sub: SubData? = null
)

@Serializable
data class SubData(
  val nilai: String? = null
)
