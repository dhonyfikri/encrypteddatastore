package com.example.encrypteddatastore.model

import kotlinx.serialization.Serializable

@Serializable
data class Data2(
  val value: String? = null,
  val sub: SubData2? = null
)

@Serializable
data class SubData2(
  val nilai: String? = null
)
