package com.example.encrypteddatastore.model

data class UserData(
  val uid: String? = null,
  val displayName: String? = null
)
