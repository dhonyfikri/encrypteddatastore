package com.example.encrypteddatastore.model

data class ProjectIncomesChartContent(
  val label: String?,
  val income: Long?,
  val outcome: Long?,
  val netIncome: Long?,
  val day: Int? = null
)
