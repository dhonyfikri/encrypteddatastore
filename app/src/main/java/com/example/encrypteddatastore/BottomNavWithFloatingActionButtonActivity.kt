package com.example.encrypteddatastore

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class BottomNavWithFloatingActionButtonActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_bottom_nav_with_floating_action_button)
  }
}