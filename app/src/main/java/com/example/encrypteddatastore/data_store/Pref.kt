package com.example.encrypteddatastore.data_store

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.MutablePreferences
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.encrypteddatastore.model.Data
import com.example.encrypteddatastore.model.Data2
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.serialization.*
import kotlinx.serialization.json.*

class Pref private constructor(private val dataStore: DataStore<Preferences>) {

  private val DATA_KEY = stringPreferencesKey("data")
  private val DATA_KEY_2 = stringPreferencesKey("data2")
  private val TOKEN = stringPreferencesKey("token")

  fun getData(): Flow<String> {
    return dataStore.data.map { preferences ->
      preferences[DATA_KEY] ?: ""
    }
  }

  fun getData2(): Flow<String> {
    return dataStore.data.map { preferences ->
      preferences[DATA_KEY_2] ?: ""
    }
  }

  suspend fun saveData(data: String) {
    dataStore.edit { preferences ->
      preferences[DATA_KEY] = data
    }
  }

  // scurity initialize
  private val security = SecurityUtil()

  /**
   * serializes data type into string
   * performs encryption
   * stores encrypted data in DataStore
   */
  private suspend inline fun <reified T> DataStore<Preferences>.secureEdit(
    value: T?,
    crossinline editStore: (MutablePreferences, String) -> Unit
  ) {
    edit {
      value?.let { data ->
        val encryptedValue =
          security.encryptData("data-store-key-alias", Json.encodeToString(data))
        editStore.invoke(it, encryptedValue.joinToString("|"))
      } ?: run {
        it.clear()
      }
    }
  }

  /**
   * fetches encrypted data from DataStore
   * performs decryption
   * deserializes data into respective data type
   */
  private inline fun <reified T> Flow<Preferences>.secureMap(
    defaultValue: T? = null,
    crossinline fetchValue: (value: Preferences) -> String
  ): Flow<T?> {
    val json = Json { encodeDefaults = true }
    return map { pref ->
      val decryptedValue: String?
      val value = fetchValue(pref)
      if (value.isNotEmpty()) {
//        try {
          decryptedValue = security.decryptData(
            "data-store-key-alias",
            value.split("|").map {
              it.toByte()
            }.toByteArray()
          )
          json.decodeFromString(decryptedValue)
//        } catch (error: Exception) {
//          Log.d("kesalahan 0", error.message.toString())
//          defaultValue
//        }
      } else {
        defaultValue
      }
    }
  }

  @Suppress("UNCHECKED_CAST")
  fun getSecureData(): Flow<Data> {
    return dataStore.data
      .secureMap(defaultValue = Data("tidak terdefinisi")) { prefs ->
        prefs[DATA_KEY].orEmpty()
      } as Flow<Data>
  }

  suspend fun saveSecureData(value: Data?) {
    dataStore.secureEdit(value) { prefs, encryptedValue ->
      prefs[DATA_KEY] = encryptedValue
    }
  }

  @Suppress("UNCHECKED_CAST")
  fun getSecureData2(): Flow<Data2> {
    return dataStore.data
      .secureMap(defaultValue = Data2("kosong")) { prefs ->
        prefs[DATA_KEY_2].orEmpty()
      } as Flow<Data2>
  }

  suspend fun saveSecureData2(value: Data2?) {
    dataStore.secureEdit(value) { prefs, encryptedValue ->
      prefs[DATA_KEY_2] = encryptedValue
    }
  }

  fun getToken(): Flow<String?> {
    return dataStore.data
      .secureMap { prefs ->
        prefs[TOKEN].orEmpty()
      }
  }

  suspend fun saveToken(value: String?) {
    dataStore.secureEdit(value) { prefs, encryptedValue ->
      prefs[TOKEN] = encryptedValue
    }
  }

  companion object {
    @Volatile
    private var INSTANCE: Pref? = null

    fun getInstance(dataStore: DataStore<Preferences>): Pref {
      return INSTANCE ?: synchronized(this) {
        val instance = Pref(dataStore)
        INSTANCE = instance
        instance
      }
    }
  }
}