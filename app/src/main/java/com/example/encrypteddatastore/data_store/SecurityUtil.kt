package com.example.encrypteddatastore.data_store

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties.BLOCK_MODE_GCM
import android.security.keystore.KeyProperties.ENCRYPTION_PADDING_NONE
import android.security.keystore.KeyProperties.KEY_ALGORITHM_AES
import android.security.keystore.KeyProperties.PURPOSE_DECRYPT
import android.security.keystore.KeyProperties.PURPOSE_ENCRYPT
import android.util.Log
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec

class SecurityUtil {
  private val provider = "AndroidKeyStore"

  private val cipher by lazy {
    Cipher.getInstance("AES/GCM/NoPadding")
  }

  private val charset by lazy {
    charset("UTF-8")
  }

  private val keyStore by lazy {
    KeyStore.getInstance(provider).apply {
      load(null)
    }
  }

  private val keyGenerator by lazy {
    KeyGenerator.getInstance(KEY_ALGORITHM_AES, provider)
  }

  fun encryptData(keyAlias: String, text: String): ByteArray {
    cipher.init(Cipher.ENCRYPT_MODE, generateSecretKey(keyAlias))
    val iv = cipher.iv
//    val ivSizeByte = ByteBuffer.allocate(Int.SIZE_BYTES).putInt(iv.size).array()
    val encryptedByte = cipher.doFinal(text.toByteArray(charset))
    return iv + encryptedByte
  }

  fun decryptData(keyAlias: String, encryptedData: ByteArray): String {
//    val ivSize = ByteBuffer.wrap(encryptedData.slice(0 until 4).toByteArray()).int.let {
//      if(it in 1..127) it else 15
//    }
    val iv = encryptedData.slice(0 until 12).toByteArray()
    val target = encryptedData.slice(12 until encryptedData.size).toByteArray()
    cipher.init(Cipher.DECRYPT_MODE, getSecretKey(keyAlias), GCMParameterSpec(128, iv))
    return cipher.doFinal(target).toString(charset)
//    return try {
//      cipher.doFinal(target).toString(charset)
//    } catch (error: Exception) {
//      throw error
//    }
  }

  private fun generateSecretKey(keyAlias: String): SecretKey {
    try {
      if (keyAlias == keyStore.aliases().nextElement()) {
        return getSecretKey(keyAlias)
      }
    } catch (e: Exception) {
      Log.e(SecurityUtil::class.java.simpleName, e.message.toString())
    }
    return keyGenerator.apply {
      init(
        KeyGenParameterSpec
          .Builder(keyAlias, PURPOSE_ENCRYPT or PURPOSE_DECRYPT)
          .setBlockModes(BLOCK_MODE_GCM)
          .setEncryptionPaddings(ENCRYPTION_PADDING_NONE)
          .build()
      )
    }.generateKey()
  }

  private fun getSecretKey(keyAlias: String): SecretKey {
    return (keyStore.getEntry(keyAlias, null) as KeyStore.SecretKeyEntry).secretKey
  }
}