package com.example.encrypteddatastore

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.encrypteddatastore.custom_view.MyChartMarkerView
import com.example.encrypteddatastore.databinding.ActivityChartBinding
import com.example.encrypteddatastore.model.ProjectIncomesChartContent
import com.example.encrypteddatastore.utils.CustomXAxisRenderer
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class ChartActivity : AppCompatActivity() {
  private lateinit var binding: ActivityChartBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityChartBinding.inflate(layoutInflater)
    setContentView(binding.root)

    setupChart(generateDummyIncomesChart())
  }

  private fun setupChart(list: List<ProjectIncomesChartContent>) {
    setupDateResearch()

    binding.chart.apply {
      setXAxisRenderer(
        CustomXAxisRenderer(
          viewPortHandler,
          xAxis,
          getTransformer(YAxis.AxisDependency.LEFT)
        )
      )
      animateX(200)
      setNoDataText("Tidak ada data")
      setNoDataTextColor(
        ContextCompat.getColor(this@ChartActivity, R.color.maroon)
      )
      setBackgroundColor(
        ContextCompat.getColor(this@ChartActivity, R.color.gray_bg)
      )
      setTouchEnabled(true)
      setDrawGridBackground(false)
      setScaleEnabled(true)
      setPinchZoom(false)

      legend.isEnabled = false
      description.isEnabled = false
      isDragEnabled = true
      axisRight.isEnabled = false
      extraBottomOffset = 10f * 1

      val mv = MyChartMarkerView(
        context,
        R.layout.chart_marker_view,
        "Mei"
      )
      marker = mv

      xAxis.apply {
        disableGridDashedLine()
        disableAxisLineDashedLine()
        setDrawGridLines(false)
        isEnabled = true
        position = XAxis.XAxisPosition.BOTTOM
        axisMinimum = 0f

        valueFormatter = object : ValueFormatter() {
          override fun getFormattedValue(value: Float): String {
            return if(value >= 1) {
              "${value.toInt()}\nMei"
            } else {
              ""
            }
          }
        }
      }

      axisLeft.apply {
        enableGridDashedLine(10f, 0f, 0f)
        axisMinimum = 0f

        valueFormatter = object : ValueFormatter() {
          override fun getFormattedValue(value: Float): String {
            return if (value > 0) {
              value.toLong().toString()
            } else {
              ""
            }
          }
        }
      }
    }

    setChartData(list)
  }

  private fun setChartData(listData: List<ProjectIncomesChartContent>) {
    val list = listData.take(20)

    val values = ArrayList<Entry>()

    list.map {
      val monthIndex = it.day
      val value = it.netIncome?.toFloat() ?: 0f
      val entry = monthIndex?.toFloat()?.let { index -> Entry(index, value) }
      entry?.let { entryValue -> values.add(entryValue) }
    }

    val set1: LineDataSet

    if (binding.chart.data != null
      && binding.chart.data.dataSetCount > 0
    ) {
      if (values.size > 1) {
        set1 = binding.chart.data.getDataSetByIndex(0) as LineDataSet
        set1.apply {
          setValues(values)
          setDrawCircles(false)
          notifyDataSetChanged()
        }

        binding.chart.apply {
            this.data.notifyDataChanged()
            notifyDataSetChanged()
          }
      } else {
        binding.chart.data = null
      }
    } else {
      if (values.size > 1) {
        set1 = LineDataSet(values, "DataSet 1")
        set1.apply {
          setDrawIcons(false)
          setDrawValues(false)
          setDrawCircles(false)
          color = ContextCompat.getColor(this@ChartActivity, R.color.maroon)
          lineWidth = 1f
          valueTextSize = 12f
          setDrawFilled(true)
          fillFormatter = IFillFormatter { _, _ ->
            binding.chart.axisLeft.axisMinimum
          }
          fillColor = ContextCompat.getColor(this@ChartActivity, R.color.chart_fill)
        }

        val dataSets = ArrayList<ILineDataSet>().apply {
          add(set1)
        }
        val lineData = LineData(dataSets)
        binding.chart.data = lineData
      }
    }

    binding.chart.apply {
      xAxis.labelCount = values.size.coerceAtMost(10)
      setVisibleXRangeMaximum(10f)
      invalidate()
    }
  }

  private fun generateDummyIncomesChart(): List<ProjectIncomesChartContent> {
    val dummyData = mutableListOf<ProjectIncomesChartContent>()

    for (day in 0..30) {
      val label = "Mei"
      val income = (40000000..100000000).random().toLong()
      val outcome = (0..income * 1 / 5).random()
      val netIncome = income - outcome

      dummyData.add(
        ProjectIncomesChartContent(
          label, income, outcome, netIncome, day
        )
      )
    }

    return dummyData
  }

  private fun setupDateResearch(){
    binding.button.setOnClickListener {
      val date1 = "2023-05-03T02:54:08.652Z".parseToDate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "UTC")
      val date2 = "2023-05-03T02:54:08.652Z".parseToDate("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "UTC")
      val date3 = Calendar.getInstance().time

      Log.d("date 1", date1.toString())
      Log.d("date 2", date2.toString())
      Log.d("date 3", date3.toString())
      Log.d("result", (date1?.compareTo(date2)).toString())

      val aku: Date? = null
      val kamu = aku?.compareTo(aku)
    }
  }

  private fun String.parseToDate(
    fromFormat: String,
    fromTimeZone: String,
  ): Date? {
    val dateParser = SimpleDateFormat(fromFormat, Locale.getDefault()).apply {
      timeZone = TimeZone.getTimeZone(fromTimeZone)
    }

    return try {
      dateParser.parse(this)
    } catch (exception: Exception) {
      exception.printStackTrace()
      null
    }
  }
}