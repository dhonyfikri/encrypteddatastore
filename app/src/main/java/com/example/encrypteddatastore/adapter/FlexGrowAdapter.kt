package com.example.encrypteddatastore.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.encrypteddatastore.databinding.ItemGrowBinding
import com.google.android.flexbox.AlignSelf
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.FlexboxLayoutManager

class FlexGrowAdapter: ListAdapter<String, FlexGrowAdapter.ViewHolder>(StringDiffCallback()) {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(
      ItemGrowBinding.inflate(
        LayoutInflater.from(parent.context), parent, false
      )
    )
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val item = getItem(position)
    holder.bind(item)
  }

  inner class ViewHolder(
    private val binding: ItemGrowBinding
  ): RecyclerView.ViewHolder(binding.root) {

    fun bind(item: String) {
      binding.run {
        val lp = root.layoutParams
        if(lp is FlexboxLayoutManager.LayoutParams){
          lp.width = 0
          lp.flexGrow = 1F
        }

        text.text = item
      }
    }
  }
}

class StringDiffCallback: DiffUtil.ItemCallback<String>() {
  override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
    return oldItem == newItem
  }

  override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
    return oldItem == newItem
  }
}