package com.example.encrypteddatastore.custom_view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.view.WindowManager
import android.widget.TextView
import androidx.core.view.isVisible
import com.example.encrypteddatastore.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.google.android.material.card.MaterialCardView

@SuppressLint("ViewConstructor")
class MyChartMarkerView(context: Context?, layoutResource: Int, private val month: String? = null) :
  MarkerView(context, layoutResource) {

  private val mContext = context
  private val container: MaterialCardView = findViewById(R.id.container)
  private val tvContent: TextView = findViewById(R.id.tvContent)

  // runs every time the MarkerView is redrawn, can be used to update the
  // content (user-interface)
  override fun refreshContent(e: Entry, highlight: Highlight?) {
    val suffix = if(month != null) " $month" else ""
    if (e is CandleEntry) {
      container.isVisible = e.high != -1f
      val text = "${e.high.toInt()}${suffix}"
      tvContent.text = text
    } else {
      container.isVisible = e.x != -1f
      val text = "${e.x.toInt()}${suffix}"
      tvContent.text = text
    }
    super.refreshContent(e, highlight)
  }

  override fun getOffset(): MPPointF {
    return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
  }

  @Suppress("Deprecation")
  override fun draw(canvas: Canvas?, posX: Float, posY: Float) {
    var mPosX = posX
    val windowManager =  mContext?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val width: Int = size.x

    val w = getWidth()
    if (width - mPosX - w < w) {
      mPosX -= w
    }
    canvas!!.translate(mPosX, posY)
    draw(canvas)
    canvas.translate(-mPosX, -posY)
  }

}