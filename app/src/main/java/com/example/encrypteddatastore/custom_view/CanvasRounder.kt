package com.example.encrypteddatastore.custom_view

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF

class CanvasRounder(
  cornerRadius: Float = 0F,
  private val topLeftRadius: Float = 0F,
  private val topRightRadius: Float = 0F,
  private val bottomLeftRadius: Float = 0F,
  private val bottomRightRadius: Float = 0F,
  cornerStrokeColor: Int = 0,
  cornerStrokeWidth: Float = 0F
) {
  private val path = Path()
  private lateinit var rectF: RectF
  private var strokePaint: Paint?
  private var cornerRadius: Float = cornerRadius
    set(value) {
      field = value
      resetPath()
    }

  init {
    if (cornerStrokeWidth <= 0)
      strokePaint = null
    else {
      strokePaint = Paint()
      strokePaint!!.style = Paint.Style.STROKE
      strokePaint!!.isAntiAlias = true
      strokePaint!!.color = cornerStrokeColor
      strokePaint!!.strokeWidth = cornerStrokeWidth
    }
  }

  fun round(canvas: Canvas, drawFunction: (Canvas) -> Unit) {
    val save = canvas.save()
    canvas.clipPath(path)
    drawFunction(canvas)
    strokePaint?.let {
      canvas.drawRoundRect(rectF, cornerRadius, cornerRadius, it)
    }
    canvas.restoreToCount(save)
  }

  fun updateSize(currentWidth: Int, currentHeight: Int) {
    rectF = RectF(0f, 0f, currentWidth.toFloat(), currentHeight.toFloat())
    resetPath()
  }

  private fun resetPath() {
    path.reset()

    if (cornerRadius != 0F) {
      path.addRoundRect(rectF, cornerRadius, cornerRadius, Path.Direction.CW)
    } else {
      val corners = floatArrayOf(
        topLeftRadius, topLeftRadius,   // Top left radius in px
        topRightRadius, topRightRadius,   // Top right radius in px
        bottomRightRadius, bottomRightRadius,     // Bottom right radius in px
        bottomLeftRadius, bottomLeftRadius      // Bottom left radius in px
      )
      path.addRoundRect(rectF, corners, Path.Direction.CW)
    }
    path.close()
  }

}