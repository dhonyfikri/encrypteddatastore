package com.example.encrypteddatastore.utils

import android.graphics.Canvas
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.renderer.XAxisRenderer
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Transformer
import com.github.mikephil.charting.utils.Utils
import com.github.mikephil.charting.utils.ViewPortHandler

class CustomXAxisRenderer(viewPortHandler: ViewPortHandler?, xAxis: XAxis?, trans: Transformer?) :
  XAxisRenderer(viewPortHandler, xAxis, trans) {
  override fun drawLabel(
    canvas: Canvas,
    formattedLabel: String,
    x: Float,
    y: Float,
    anchor: MPPointF,
    angleDegrees: Float
  ) {
    val lines = formattedLabel.split("\n")
    for (i in lines.indices) {
      val vOffset = i * mAxisLabelPaint.textSize
      Utils.drawXAxisValue(
        canvas, lines[i], x, y + vOffset, mAxisLabelPaint, anchor, angleDegrees
      )
    }
  }
}