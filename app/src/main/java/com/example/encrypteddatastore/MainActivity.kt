package com.example.encrypteddatastore

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.encrypteddatastore.adapter.FlexGrowAdapter
import com.example.encrypteddatastore.data_store.Pref
import com.example.encrypteddatastore.databinding.ActivityMainBinding
import com.example.encrypteddatastore.model.Data
import com.example.encrypteddatastore.model.Data2
import com.example.encrypteddatastore.model.SubData
import com.example.encrypteddatastore.model.SubData2
import com.example.encrypteddatastore.model.UserData
import com.example.encrypteddatastore.utils.dataStore
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.onesignal.OneSignal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
  private lateinit var binding: ActivityMainBinding
  private lateinit var pref: Pref

  private lateinit var callbackManager: CallbackManager
  private lateinit var auth: FirebaseAuth
  private lateinit var database: FirebaseDatabase

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityMainBinding.inflate(layoutInflater)
    setContentView(binding.root)

    @Suppress("DEPRECATION")
    FacebookSdk.sdkInitialize(applicationContext)
    auth = Firebase.auth
    database = FirebaseDatabase.getInstance("https://authapp-ed834-default-rtdb.firebaseio.com/")

    pref = Pref.getInstance(dataStore)

    binding.apply {
      btnPage2.setOnClickListener {
//        lifecycleScope.launch {
////          OneSignal.disablePush(!pref.getToken().first().isNullOrBlank())
//          pref.saveToken(
//            if (pref.getToken().first().isNullOrBlank()) {
//              "token"
//            } else {
//              ""
//            }
//          )
//        }


      }

      lifecycleScope.launch {
        repeatOnLifecycle(Lifecycle.State.STARTED) {
          pref.getToken().onEach {
            Toast.makeText(
              this@MainActivity,
              if (it.isNullOrBlank()) "Tidak Login" else "Sudah Login",
              Toast.LENGTH_SHORT
            ).show()
          }.launchIn(this)
        }
      }

      //OneSignal
      OneSignal.setEmail("dakocan@gmail.com")



      btnSave.setOnClickListener {
        setData(
          Data(
            value = etData.text.toString(),
            sub = SubData(etData.text.toString())
          )
        )
      }

      btnSave.setOnLongClickListener {
        setData(null)
        return@setOnLongClickListener true
      }

      btnLoad.setOnClickListener {
        lifecycleScope.launch(Dispatchers.Main) {
          val data = getData().first()
          tvResult.text = data.sub?.nilai.toString()
        }
      }

      btnLoad.setOnLongClickListener {
        lifecycleScope.launch(Dispatchers.Main) {
          val data = pref.getData().first()
          tvResult.text = data
        }

        return@setOnLongClickListener true
      }


      btnSave2.setOnClickListener {
        setData2(Data2(value = etData2.text.toString(), sub = SubData2(etData2.text.toString())))
      }

      btnSave2.setOnLongClickListener {
        setData2(null)
        return@setOnLongClickListener true
      }

      btnLoad2.setOnClickListener {
        lifecycleScope.launch(Dispatchers.Main) {
          val data = getData2().first()
          tvResult2.text = data.sub?.nilai.toString()
        }
      }

      btnLoad2.setOnLongClickListener {
        lifecycleScope.launch(Dispatchers.Main) {
          val data = pref.getData2().first()
          tvResult2.text = data
        }

        return@setOnLongClickListener true
      }
    }

    setupFacebookButtonLogin()

    setAdapter()
  }

  private fun setAdapter() {
    val adapter = FlexGrowAdapter()
    adapter.submitList(
      listOf(
        "Satu",
        "Dua",
        "Tiga",
        "Empat",
        "Lima",
        "Enam",
      )
    )

    val layoutManager = FlexboxLayoutManager(this)
    layoutManager.flexDirection = FlexDirection.ROW
    binding.listFlex.layoutManager = layoutManager

    binding.listFlex.adapter = adapter
  }

  private fun getData() = pref.getSecureData()

  private fun setData(data: Data?) {
    lifecycleScope.launch {
      pref.saveSecureData(data)
    }
  }

  private fun getData2() = pref.getSecureData2()

  private fun setData2(data: Data2?) {
    lifecycleScope.launch {
      pref.saveSecureData2(data)
    }
  }

  private fun setupFacebookButtonLogin() {
    callbackManager = CallbackManager.Factory.create()
    LoginManager.getInstance()
      .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
        override fun onCancel() {
          Toast.makeText(this@MainActivity, "Login Batal...", Toast.LENGTH_SHORT).show()
        }

        override fun onError(error: FacebookException) {
          Log.d("Gagal facebook", error.message.toString())
          Toast.makeText(this@MainActivity, "Login Gagal...", Toast.LENGTH_SHORT).show()
        }

        override fun onSuccess(result: LoginResult) {
          Toast.makeText(this@MainActivity, "Login Sukses...", Toast.LENGTH_SHORT).show()
          handleFacebookAccessToken(result.accessToken)
        }

      })

    if (auth.currentUser != null) {
      binding.btnFacebookLogin.text = "Facebook - Log Out"
    }

    binding.btnFacebookLogin.setOnClickListener {
      auth.currentUser?.uid?.let {
        LoginManager.getInstance().logOut()
        binding.btnFacebookLogin.text = "Facebook - Login"
        auth.currentUser?.delete()?.addOnCompleteListener {
          Log.i("sebabnya", it.isSuccessful.toString())
        }
        database.reference.child("users").child(it).removeValue()
      } ?: run {
        LoginManager.getInstance()
          .logInWithReadPermissions(this, callbackManager, listOf("public_profile"))
      }
    }

    binding.btnFacebookLogin.setOnLongClickListener {
      LoginManager.getInstance()
        .logInWithReadPermissions(this, callbackManager, listOf("public_profile"))
      return@setOnLongClickListener true
    }
  }

  private fun handleFacebookAccessToken(token: AccessToken) {
    Log.d("Facebook", "handleFacebookAccessToken:$token")

    val credential = FacebookAuthProvider.getCredential(token.token)
    auth.signInWithCredential(credential)
      .addOnCompleteListener(this) { task ->
        if (task.isSuccessful) {
          // Sign in success, update UI with the signed-in user's information
          Log.d("Facebook", "signInWithCredential:success")
          val user = auth.currentUser
          user?.let {
            Toast.makeText(this, "Welcome back ${user.displayName}", Toast.LENGTH_SHORT).show()
            Log.d("datanya", user.displayName.toString())
            Log.d("datanya", user.email.toString())
            Log.d("datanya", user.phoneNumber.toString())
            val userData = UserData(uid = user.uid, displayName = user.displayName)
            database.reference.child("users").child(userData.uid ?: "").setValue(userData)
            binding.btnFacebookLogin.text = "Facebook - Log Out"
          } ?: run {
            Toast.makeText(this, "User null", Toast.LENGTH_SHORT).show()
          }
        } else {
          // If sign in fails, display a message to the user.
          Log.w("Facebook", "signInWithCredential:failure", task.exception)
          Toast.makeText(
            baseContext, "Authentication failed.",
            Toast.LENGTH_SHORT
          ).show()
        }
      }
  }
}